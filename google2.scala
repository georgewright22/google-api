//to do: await http requests to finish, otherwise throws exception when run as script. 
//only works being pasted into the ammonite shell

//REQ: add in secret and client id

interp.load.ivy("org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.1")
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import $ivy.`com.lihaoyi:ammonite-shell_2.12.7:1.3.2-2-3da4986`
import $ivy.`com.lihaoyi:ammonite-ops_2.12:1.3.2-2-3da4986`
import $ivy.`ch.qos.logback:logback-classic:1.2.3`
import $ivy.`org.dispatchhttp::dispatch-core:0.14.0`
import $ivy.`org.scala-lang:scala-parser-combinators:2.11.0-M4`
import dispatch._, Defaults._
import scala.util.parsing.json._
import ammonite.ops._


val client_id = ""
val client_secret = ""
val scope = "https://www.googleapis.com/auth/admin.directory.user.readonly"
val refresh_token = "1/VMGCuuXsADR1HzUFRnClMp00cBl99_L0TelA-OGlM8PpjuRfFbZGBNxr7lnH2DdZ"
val refresh = url("https://www.googleapis.com/oauth2/v4/token").addParameter("client_id", client_id).addParameter("client_secret", client_secret).addParameter("grant_type", "refresh_token").addParameter("refresh_token", refresh_token).POST

//initialize variable to hold the access_token
var access_token_holder  = ""

//refresh access token and save
Http(refresh).foreach(x => access_token_holder = x.getResponseBody)
@
val access_token_raw = JSON.parseFull(access_token_holder).get.asInstanceOf[Map[String, Any]]
val access_token: String = access_token_raw("access_token").asInstanceOf[String]
val get_users = url("https://www.googleapis.com/admin/directory/v1/users").addQueryParameter("access_token", access_token).addQueryParameter("domain", "virginvoyages.com").addQueryParameter("maxResults", "500")

//initialize var to hold user list
var user_list = ""

//query google api to get users
Http(get_users).foreach(res => user_list = res.getResponseBody)
@
val date = LocalDateTime.now.format(DateTimeFormatter.ofPattern("MM-dd-YYYY_HH.mm.ss"))
//save user list to file in current dir
write("google-users" + date + ".json")
